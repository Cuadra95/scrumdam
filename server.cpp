/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <fstream>
#include <iostream>
#include <tinyxml2.h>
using namespace tinyxml2;
using namespace std;

void error(const char *msg)
{
	perror(msg);
	exit(1);
}



int main(int argc, char *argv[])
{
	int sockfd, newsockfd, portno;
	socklen_t clilen;
	struct sockaddr_in serv_addr, cli_addr;
	int n;
	XMLDocument xmlDoc;
	XMLPrinter printer;
	xmlDoc.Print(&printer);
	//char buffer[256];
	char *buffer = (char *)printer.CStr();
	

	if (argc < 2) {
		fprintf(stderr,"ERROR, no port provided\n");
		exit(1);
	}
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) 
		error("ERROR opening socket");
	bzero((char *) &serv_addr, sizeof(serv_addr));
	portno = atoi(argv[1]);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portno);
	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) 
		error("ERROR on binding");
	listen(sockfd,5);
	clilen = sizeof(cli_addr);
	newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
	if (newsockfd < 0) 
		error("ERROR on accept");
	bzero(buffer,256);

	//TODO CONECATDO:
	while(1){

		//LECTURA DE MENSAJE DEL CLIENTE
		n = read(newsockfd,buffer,255);

		//Check error
		if (n < 0) error("ERROR reading from socket");


		//BUFFER CONTIENE EL MENSAJE RECIBIDO:
		printf("Here is the message: %s\n",buffer);
		

		//Crear xml en memoria
		XMLDocument xmlMensaje;
		//XMLError eResult = xmlMensaje.LoadFile("andres.xml");
		//leo e igualo el buffer al documento xmlMensaje
		xmlMensaje.Parse( buffer );
		//Lectura de primero elemento de xmlMensaje
		XMLNode * pRoot = xmlMensaje.FirstChild();
		XMLElement * pElement = pRoot->FirstChildElement("CANTIDAD");
		if(pElement == NULL) return XML_ERROR_PARSING_ELEMENT;
		//variable para igualar primer elemento
		int cantidadMatriz;
		pElement->QueryIntText(&cantidadMatriz);
		//lectura de segundo elemento de xmlMensaje
		pElement = pRoot->FirstChildElement("NUMERO");
		if(pElement == NULL) return XML_ERROR_PARSING_ELEMENT;
		//variable para igualar segundo elemento
		int numeroMatriz;
		pElement->QueryIntText(&numeroMatriz);
		//Crear documento .txt para introducir Matriz
		printf("LECTURA XML: %d %d \n", cantidadMatriz, numeroMatriz);
		
		//variable para determinar el centro de la matriz
		int centro = cantidadMatriz/2;


		string cadena1 = "";
		string cadena0 = "";
		std::string cadenaTotal = "";


		if (numeroMatriz == 1){
				
			for(int x = 1; x <= centro; x++){
				cadena1 = cadena1 + "1";				
			}
			for (int j = 1; j <= centro; j++){
				cadena0 = cadena0 + "0";
			}
		}	
		if (numeroMatriz == 0){			
			
			for(int x = 1; x <= centro; x++){
				cadena1 = cadena1 + "0";				
			}
			for (int j = 1; j <= centro; j++){
				cadena0 = cadena0 + "1";
			}
		}
		cadenaTotal = cadena1 + cadena0;
		FILE *salidaMatriz;
		salidaMatriz = fopen("salidaMatriz.txt", "w");
		for (int i = 1; i <= cantidadMatriz; i++){
			std::cout << cadenaTotal << "\n";
			fprintf(salidaMatriz, "%s\n", cadenaTotal.c_str());
		}
		fclose(salidaMatriz);





		//Mensaje ReSPUESTA para el cliente
		n = write(newsockfd,"I got your message",18);

		if (n < 0) error("ERROR writing to socket");
	}
	close(newsockfd);
	close(sockfd);
	return 0;
}
