#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <fstream>
#include <iostream>
#include <tinyxml2.h>
#include <string>
#include <cstring>



using std::cout; 
using std::cin; 
using std::endl; 
using std::fstream; 
using std::string; 
using std::ifstream;   
using namespace tinyxml2;
using namespace std;

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[])
{

    
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    char *buffer;
    XMLDocument xmlDoc;
    
    
    if (argc < 3) {
       fprintf(stderr,"usage %s hostname port\n", argv[0]);
       exit(0);
    }
    portno = atoi(argv[2]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);



    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
        error("ERROR connecting");


    printf("Please enter the message: ");

    ifstream file;
    file.open("matriz.txt");

    

    char cantidadMatriz[256];
    file >> cantidadMatriz;
    int cero;
    int contador = 0;
    char numero = cantidadMatriz[0];
    char aux [20];
    sprintf(aux, "%c", numero);

    //if(cantidadMatriz == '1')

    for(int i =0; i < strlen(cantidadMatriz); i++){
        file >> cantidadMatriz;
        contador++;
    }
    int total = contador;

     
    cout << total << endl; 
    cout <<  numero << endl;



    //Tinyxml2
    XMLNode * pRoot = xmlDoc.NewElement("Matriz");
    xmlDoc.InsertFirstChild(pRoot);
    XMLElement * pElement = xmlDoc.NewElement("CANTIDAD");
    pElement->SetText(total);
    pRoot->InsertEndChild(pElement);
    pElement = xmlDoc.NewElement("NUMERO");
    pElement->SetText(aux);
    pRoot->InsertEndChild(pElement);

    //  xmlDoc.SaveFile("SavedData.xml");

    //ENVIO DEL XML 
    XMLPrinter printer;
    xmlDoc.Print(&printer);
    buffer = (char *)printer.CStr();

    n = write(sockfd,buffer,strlen(buffer));
    
    if (n < 0) 
         error("ERROR writing to socket");
    bzero(buffer,256);
    n = read(sockfd,buffer,255);
    if (n < 0) 
         error("ERROR reading from socket");
    printf("%s\n",buffer);
   // close(sockfd);
    return 0;
}
